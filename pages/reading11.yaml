title:      "Reading 11: Filesystems"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will begin our exploration of [filesystems] and in particular
    the venerable [FFS] and [LFS].

    <div class="alert alert-info" markdown="1">

    #### <i class="fa fa-search"></i> TL;DR

    For this reading assignment, you are to read about [filesystems], and
    submit your responses to the [Reading 11 Quiz].

    </div>

    [filesystems]:  https://en.wikipedia.org/wiki/File_system
    [FFS]:          https://en.wikipedia.org/wiki/Unix_File_System
    [LFS]:          https://en.wikipedia.org/wiki/Log-structured_File_System_(BSD)

    <img src="static/img/ostep.jpg" class="pull-right">

    ## Reading

    The readings for **Tuesday, November 21** are:

    1. [Operating Systems: Three Easy Pieces]

        - <p>[File System Implementation](http://pages.cs.wisc.edu/~remzi/OSTEP/file-implementation.pdf)</p>
        - <p>[Locality and The Fast File System](http://pages.cs.wisc.edu/~remzi/OSTEP/file-ffs.pdf)</p>
        - <p>[Log-structured File Systems](http://pages.cs.wisc.edu/~remzi/OSTEP/file-lfs.pdf)</p>

    ## Quiz

    Once you have done the readings, answer the following [Reading 11 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading11.json');
    </script>

    ## Submission

    To submit you work, follow the same process outlined in [Reading 00]:

        :::bash
        $ git checkout master                 # Make sure we are in master branch
        $ git pull --rebase                   # Make sure we are up-to-date with GitLab

        $ git checkout -b reading11           # Create reading11 branch and check it out

        $ cd reading11                        # Go into reading11 folder
        $ $EDITOR answers.json                # Edit your answers.json file

        $ ../.scripts/submit.py               # Check reading11 quiz
        Submitting reading11 assignment ...
        Submitting reading11 quiz ...
             Q01 0.80
             Q02 0.50
             Q03 0.30
             Q04 0.30
             Q05 0.40
             Q06 0.20
             Q07 0.50
           Score 3.00

        $ git add answers.json                # Add answers.json to staging area
        $ git commit -m "Reading 11: Done"    # Commit work

        $ git push -u origin reading11        # Push branch to GitLab

    Remember to [create a merge request] and assign the appropriate TA from the
    [Reading 11 TA List].

    [busybox]:                                  https://www.busybox.net/
    [Operating Systems: Three Easy Pieces]:     http://pages.cs.wisc.edu/~remzi/OSTEP/
    [GitLab]:                                   https://gitlab.com
    [Reading 00]:                               reading00.html
    [Reading 11 Quiz]:                          static/json/reading11.json
    [JSON]:                                     http://www.json.org/
    [git-branch]:                               https://git-scm.com/docs/git-branch
    [dredd]:                                    https://dredd.h4x0r.space
    [create a merge request]:                   https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 11 TA List]:                       reading11_tas.html
