title:      "Reading 05: Locks and Condition Variables"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Last week, we have studied two different forms of [concurrency]:
      [event-based programming] via [select]/[poll] and [threads] (ala
      [pthreads]).  While the former allowed us to overlap I/O and computation,
      it did not allow us to take advantage of parallelism.  To do harness
      multiple hardware resources, we will need to utilize [threads].
      Unfortunately, using [threads] also means we need to employ [locks] and
      [condition variables] to ensure correct concurrent access to shared
      resources.

    The readings this week focus on how [locks] and [condition variables] work
    and how we can use them to implement [concurrent data structures].

    <div class="alert alert-info" markdown="1">

    #### <i class="fa fa-search"></i> TL;DR

    For this reading assignment, you are to read about using [locks] and
    [condition variables] to synchronize [pthreads] and submit your responses
    to the [Reading 05 Quiz].

    </div>

    [concurrency]: https://en.wikipedia.org/wiki/Concurrency_(computer_science)
    [event-based programming]: https://en.wikipedia.org/wiki/Event-driven_programming
    [threads]: https://en.wikipedia.org/wiki/Thread_(computing)
    [select]: http://man7.org/linux/man-pages/man2/select.2.html
    [poll]: http://man7.org/linux/man-pages/man2/poll.2.html
    [multiprogramming]: https://en.wikipedia.org/wiki/Computer_multitasking
    [pthreads]: http://man7.org/linux/man-pages/man7/pthreads.7.html
    [locks]: https://en.wikipedia.org/wiki/Lock_(computer_science)
    [condition variables]: https://en.wikipedia.org/wiki/Monitor_(synchronization)
    [semaphores]: https://en.wikipedia.org/wiki/Semaphore_(programming)
    [concurrent data structures]: https://en.wikipedia.org/wiki/Concurrent_data_structure

    <img src="static/img/ostep.jpg" class="pull-right">

    ## Reading

    The readings for **Tuesday, September 26** are:

    1. [Operating Systems: Three Easy Pieces]

        1. [Locks](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks.pdf)
        2. [Lock-based Concurrent Data Structures](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks-usage.pdf)
        3. [Condition Variables](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-cv.pdf)

    ### Optional Readings

    The following are optional related readings:

    1. [Simple, Fast, and Practical Non-Blocking and Blocking Concurrent Queue Algorithms](http://www.cs.rochester.edu/research/synchronization/pseudocode/queues.html)

    2. [The Producer/Consumer Problem](https://docs.oracle.com/cd/E19455-01/806-5257/sync-31/index.html)

    3. [Concurrency in C++11](https://www.classes.cs.uchicago.edu/archive/2013/spring/12300-1/labs/lab6/)

    4. [Multithreading: Producer Consumer Problem](http://austingwalters.com/multithreading-producer-consumer-problem/)

    <div class="alert alert-warning" markdown="1">
    #### <i class="fa fa-code"></i> C++11 Threads

    Although the textbook and class examples will revolve around [pthreads],
    you may also utilize the [threading library] introduced in [C++11].

    </div>

    [threading library]:    http://en.cppreference.com/w/cpp/thread
    [C++11]:                https://en.wikipedia.org/wiki/C%2B%2B11

    ## Quiz

    Once you have done the readings, answer the following [Reading 05 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading05.json');
    </script>

    ## Submission

    To submit you work, follow the same process outlined in [Reading 00]:

        :::bash
        $ git checkout master                 # Make sure we are in master branch
        $ git pull --rebase                   # Make sure we are up-to-date with GitLab

        $ git checkout -b reading05           # Create reading05 branch and check it out

        $ cd reading05                        # Go into reading05 folder
        $ $EDITOR answers.json                # Edit your answers.json file

        $ ../.scripts/submit.py               # Check reading05 quiz
        Submitting reading05 assignment ...
        Submitting reading05 quiz ...
             Q01 0.50
             Q02 0.20
             Q03 0.30
             Q04 0.50
             Q05 0.30
             Q06 0.20
             Q07 0.20
             Q08 0.20
             Q09 0.20
             Q10 0.40
           Score 3.00

        $ git add answers.json                # Add answers.json to staging area
        $ git commit -m "Reading 05: Done"    # Commit work

        $ git push -u origin reading05        # Push branch to GitLab

    Remember to [create a merge request] and assign the appropriate TA from the
    [Reading 05 TA List].

    [busybox]:                                  https://www.busybox.net/
    [Operating Systems: Three Easy Pieces]:     http://pages.cs.wisc.edu/~remzi/OSTEP/
    [GitLab]:                                   https://gitlab.com
    [Reading 00]:                               reading00.html
    [Reading 05 Quiz]:                          static/json/reading05.json
    [JSON]:                                     http://www.json.org/
    [git-branch]:                               https://git-scm.com/docs/git-branch
    [dredd]:                                    https://dredd.h4x0r.space
    [create a merge request]:                   https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 05 TA List]:                       reading05_tas.html
