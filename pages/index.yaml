title:      Operating System Principles
icon:       fa-desktop
navigation:
    - name: "Logistics"
      link: "#logistics"
      icon: "fa-cogs"
    - name: "Schedule"
      link: "#schedule"
      icon: "fa-calendar"
    - name: "Requirements"
      link: "#Requirements"
      icon: "fa-balance-scale"
    - name: "Resources"
      link: "#resources"
      icon: "fa-external-link"
internal:
external:
    tas:        'static/yaml/ta.yaml'
    schedule:   'static/yaml/schedule.yaml'
body:       |

    <div class="row" markdown="1">

    **CSE 30341** is the one of the core classes in the [Computer Science and
    Engineering] program at the [University of Notre Dame].  This course
    introduces all aspects of modern operating systems. Topics include process
    structure and synchronization, interprocess communication, memory
    management, file systems, security, I/O, and distributed files systems.

    </div>

    <img src="static/img/house.png" class="pull-right" style="margin-left: 10px;">

    <div class="row" markdown="1">

    Upon successful completion of this course, students will be able to:

    - **Describe** the basic components of a modern operating system.

    - **Understand** the symbiotic relationship between computer architecture
      and operating system design.

    - **Discuss**  how operating systems provide abstractions for
      virtualization, concurrency, and persistence.

    - **Construct** applications that utilize processes, threads, and sockets
      to solve problems requiring concurrent or parallel computation.

    - **Explain** how resources such as memory is allocated and managed by the
      operating system.

    - **Evaluate** the trade-offs embedded in different operating system
      techniques, algorithms, and data structures.

    - **Analyze** the performance of applications in a variety of system contexts.

    [Computer Science and Engineering]:     http://cse.nd.edu
    [University of Notre Dame]:             http://www.nd.edu

    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="logistics"><i class="fa fa-cogs"></i> Logistics</h2>
        </div>
    </div>

    <div class="row">
    <div class="col-md-6">
    <div class="row">
    <div class="col-md-6">
        <h3>Class Information</h3>
        <dl>
            <dt><i class="fa fa-clock-o"></i> Lecture</dt>
            <dd><strong>T/TH</strong> 9:30 AM - 10:45 AM</dd>

            <dt><i class="fa fa-home"></i> Location</dt>
            <dd>136 DeBartolo Hall</dd>

            <dt><i class="fa fa-envelope"></i> Mailing List (Class)</dt>
            <dd><a href="mailto:fa17-cse-30341-01-group@nd.edu">fa17-cse-30341-01-group@nd.edu</a></dd>

            <dt><i class="fa fa-envelope"></i> Mailing List (Staff)</dt>
            <dd><a href="mailto:fa17-cse-30341-01-staff-list@nd.edu">fa17-cse-30341-01-staff-list@nd.edu</a></dd>

            <dt><i class="fa fa-slack"></i> Slack</dt>
            <dd><a href="https://nd-cse.slack.com/messages/cse-30341-fa17/">#cse-30341-fa17</a></dd>

            <dt><i class="fa fa-gitlab"></i> GitLab</dt>
            <dd><a href="https://www.gitlab.com/nd-cse-30341-fa17/">nd-cse-30341-fa17</a></dd>
        </dl>
    </div>

    <div class="col-md-6">
        <h3>Instructor</h3>

        <dl>
            <dt><i class="fa fa-user"></i> Instructor</dt>
            <dd><a href="http://www3.nd.edu/~pbui/">Peter Bui</a> (<a href="mailto:pbui@nd.edu">pbui@nd.edu</a>)</dd>

            <dt><i class="fa fa-life-ring"></i> Office Hours</dt>
            <dd><strong>M/W/F</strong> 2:00 PM - 4:30 PM, and by <strong>appointment</strong></dd>

            <dt><i class="fa fa-briefcase"></i> Office Location</dt>
            <dd>350 Fitzpatrick Hall</dd>
        </dl>
    </div>
    </div>

    <div class="row">
      <div class="alert alert-info">
            <h4><i class="fa fa-question"></i> Help Protocol</h4>
            <ol class="list-inline">
                <li><em><i class="fa fa-lightbulb-o"></i> Think</em></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><strong><i class="fa fa-slack"></i> Slack</strong></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><em><i class="fa fa-lightbulb-o"></i> Think</em></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><strong><i class="fa fa-envelope"></i> Email</strong></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><em><i class="fa fa-lightbulb-o"></i> Think</em></li>
                <li><small><i class="fa fa-arrow-right"></i></small></li>
                <li><strong><i class="fa fa-briefcase"></i> Office</strong></li>
            </ol>
      </div>
    </div>
    </div>

    <div class="col-md-6">
    <h3>Teaching Assistants</h3>

      <div class="row">
        {% for index, ta in enumerate(sorted(page.external['tas'], key=lambda ta: ta['name'].split()[-1])) %}
          <div class="col-md-4">
            <dl>
              <dt><i class="fa fa-user"></i> Teaching Assistant</dt>
              <dd>{{ ta['name'] }} (<a href="mailto:{{ ta['netid'] }}@nd.edu">{{ ta['netid'] }}@nd.edu</a>)</dd>
              <dt><i class="fa fa-life-ring"></i> Office Hours</dt>
              <dd>{{ ta.get('hours', 'TBD') }}</dd>
              <dt><i class="fa fa-briefcase"></i> Office Location</dt>
              <dd>{{ ta.get('location', 'TBD') }}</dd>
            </dl>
            </div>
          {% if (index + 1) % 3 == 0 %}
          </div><div class="row">
          {% end %}
        {% end %}
      </div>
    </div>
    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="schedule"><i class="fa fa-calendar"></i> Schedule</h2>
        </div>
    </div>

    <style>
    table.schedule td.unit {
        background-color: #fff;
        border-right: 2px solid #ddd;
        text-align: right;
        width: 175px;
        vertical-align: middle;
    }
    </style>

    <div class="row">
        <table class="table condensed table-striped schedule">
            <thead>
                <tr>
                    <th class="text-center">Unit</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Topics</th>
                    <th class="text-center" style="width: 125px;">Assignment</th>
                </tr>
            </thead>
            <tbody>

                {% for theme in page.external['schedule'] %}
                {% if 'break' in theme['name'].lower() %}
                  <tr>
                  <td colspan="4" class="text-center text-strong" style="border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; padding: 4px;">{{ theme['name'] }}</td>
                  </tr>
                  {% continue %}
                {% end %}

                {% for index, day in enumerate(theme['days']) %}
                <tr>
                {% if index == 0 %}
                    <td rowspan="{{ len(theme['days']) }}" class="text-strong text-primary text-center unit">{{ theme['name'] }}</td>
                {% end %}
                    <td class="text-center text-strong">{{ day['date'] }}</td>
                    <td>
                    {{ day.get('topics', '') }}
                    {% for item in day.get('items', []) %}
                    {% if item['name'].lower().endswith('exam') %}
                      {% set item['label'] = 'danger' %}
                    {% elif item['name'].lower().startswith('project') %}
                      {% set item['label'] = 'caution' %}
                    {% elif item['name'].lower().startswith('checklist') %}
                      {% set item['label'] = 'warning' %}
                    {% else %}
                      {% set item['label'] = 'default' %}
                    {% end %}
                    {% if 'link' in item %}
                      <a class="label label-{{ item['label'] }}" href="{{ item['link'] }}">{{ item['name'] }}</a>
                    {% else %}
                      <span class="label label-{{ item['label'] }}">{{ item['name'] }}</span>
                    {% end %}
                    {% end %}
                    </td>
                    <td class="text-strong">
                    {% set assignment = day.get('assignment') %}
                    {% if assignment %}
                    {% set assignment_id = ''.join(assignment.lower().split()) %}
                    {% if assignment_id.startswith('reading') %}
                      {% set assignment_label = 'primary' %}
                    {% else %}
                      {% set assignment_label = 'caution' %}
                    {% end %}
                      <a href="{{ assignment_id }}.html" class="label label-{{ assignment_label }}">{{ assignment }}</a>
                    {% end %}
                    </td>
                </tr>
                {% end %}
                {% end %}

            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="Requirements"><i class="fa fa-balance-scale"></i> Requirements</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
        <h3>Coursework</h3>
        <table class="table table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th class="text-center">Component</th>
                    <th class="text-center">Points</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><span class="label label-primary">Readings</span> Weekly reading assignments.</td>
                    <td class="text-center">12 &times; 3</td>
                </tr>
                <tr>
                    <td><span class="label label-caution">Projects</span> Periodic group projects.</td>
                    <td class="text-center">6 &times; 24</td>
                </tr>
                <tr>
                    <td><span class="label label-danger">Exams</span> Midterm and Final Exams.</td>
                    <td class="text-center">50 + 70</td>
                </tr>
                <tr>
                    <td class="text-strong text-right">Total</td>
                    <td class="text-strong text-center">300</td>
                </tr>
            </tbody>
        </table>
        </div>
        <div class="col-md-6">
        <h3>Grading</h3>
        <table class="table table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Grade</th>
                    <th class="text-center">Points</th>
                    <th class="text-center">Grade</th>
                    <th class="text-center">Points</th>
                    <th class="text-center">Grade</th>
                    <th class="text-center">Points</th>
                </tr>
            </thead>
            <tbody>
                <tr class="success">
                    <td></td>
                    <td></td>
                    <td class="text-center text-strong">A</td>
                    <td class="text-center">280-300</td>
                    <td class="text-center text-strong">A-</td>
                    <td class="text-center">270-279</td>
                </tr>
                <tr class="info">
                    <td class="text-center text-strong">B+</td>
                    <td class="text-center">260-269</td>
                    <td class="text-center text-strong">B</td>
                    <td class="text-center">250-259</td>
                    <td class="text-center text-strong">B-</td>
                    <td class="text-center">240-249</td>
                </tr>
                <tr class="warning">
                    <td class="text-center text-strong">C+</td>
                    <td class="text-center">230-239</td>
                    <td class="text-center text-strong">C</td>
                    <td class="text-center">220-229</td>
                    <td class="text-center text-strong">C-</td>
                    <td class="text-center">210-219</td>
                </tr>
                <tr class="danger">
                    <td class="text-center text-strong">D</td>
                    <td class="text-center">195-209</td>
                    <td class="text-center text-strong">F</td>
                    <td class="text-center">0-194</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>


    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="alert alert-info">
                <h4><i class="fa fa-calendar"></i> Due Dates</h4>
                <p>
                All <b>Readings</b> are to be
                  submitted to your own <b>private</b> <a
                  href="https://gitlab.com">GitLab</a> repository.  Unless
                  specified otherwise:
                <ul>
                <li><b>Readings</b> are due by <b>noon</b> on the <b>Monday</b> of each week.</li>
                <br>
                <li><b>Projects</b> are due by <b>noon</b> on the <b>Saturday</b> of each week.</li>
                </ul>
                </p>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
          <h3>Policies</h3>
          <div class="row">
              <div class="col-md-4">
              <h4>Participation</h4>
              <p>
              Students are expected to attend and contribute regularly in class. This
              means answering questions in class, participating in discussions, and
              helping other students.
              </p>
              <p>
              Foreseeable absences should be discussed with the instructor ahead of time.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Academic Honesty</h4>
              <p>
              Any academic misconduct in this course is considered a serious
              offense, and the strongest possible academic penalties will be
              pursued for such behavior. Students may discuss high-level
              ideas with other students, but at the time of implementation
              (i.e. programming), each person must do his/her own work. Use
              of the Internet as a reference is allowed but directly copying
              code or other information is cheating. It is cheating to copy,
              to allow another person to copy, all or part of an exam or a
              assignment, or to fake program output. It is also a violation
              of the <a href="https://honorcode.nd.edu/">Undergraduate
              Academic Code of Honor</a> to observe and then fail to report
              academic dishonesty. You are responsible for the security and
              integrity of your own work.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Late Work</h4>
              <p>
              In the case of a serious illness or other excused absence, as defined by
              university policies, coursework submissions will be accepted late by the
              same number of days as the excused absence.
              </p>
              <p>
              Otherwise, there is a penalty of 25% per day late (except where noted). You
              may submit some parts of an assignment on time and some parts late. Each
              submission must clearly state which parts it contains; no part can be
              submitted more than once.
              </p>
              </div>
              <div class="col-md-4">
              <h4>Students with Disabilities</h4>
              <p>
              Any student who has a documented disability and is registered with
              Disability Services should speak with the professor as soon as possible
              regarding accommodations. Students who are not registered should contact
              the <a href="http://disabilityservices.nd.edu/">Office of Disabilities</a>.
              </p>
              </div>
          </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="alert alert-danger">
                <h4><i class="fa fa-warning"></i> CSE Guide to the Honor Code</h4>
                <p>
                For the assignments in this class, you may discuss with other
                students and consult printed and online resources.  You may
                quote from books and online sources as long as you <b>cite them
                properly</b>.  However, you may <b>not</b> look at another
                student's solution, and you may <b>not</b> look at solutions.
                </p>
                <center>
                <img src="static/img/honor-code-RARCSNSN.png" style="padding: 5px" class="img-responsive">
                </center>
                <p>
                For further guidance please refer to the <a
                href="https://cse.nd.edu/academics/honor-code">CSE Honor
                Code</a> or ask the instructor.
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="page-header">
            <h2 id="resources"><i class="fa fa-external-link"></i> Resources</h2>
        </div>
    </div>

    <div class="row" markdown="1">
    <div class="col-md-4" markdown="1">
    ### Textbooks

    <div class="thumbnail">
      <img src="static/img/ostep.jpg" width="125px">
      <div class="caption">
          <h4>Operating Systems: Three Easy Pieces </h4>
          <p>Remzi Arpaci-Dusseau, Andrea Arpaci-Dusseau <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/" class="label label-primary pull-right">Online Version</a>
          </p>
      </div>
    </div>

    </div>
    <div class="col-md-4" markdown="1">
    ### Unix Tutorials

    - [Learning the Shell](http://linuxcommand.org/lc3_learning_the_shell.php)
    - [UNIX / Linux Tutorial for Beginners](http://www.ee.surrey.ac.uk/Teaching/Unix/)
    - [FreeBSD Handbook - UNIX Basics](https://www.freebsd.org/doc/handbook/basics.html)

    ### Git Tutorials

    - [Git-Scm](https://git-scm.com/)
    - [Pro Git](https://git-scm.com/book/en/v2)
    - [Atlassian Getting Started Git Tutorials](https://www.atlassian.com/git/tutorials)
    - [gittutorial](http://git-scm.com/docs/gittutorial)
    - [Code School - Try Git](https://try.github.io/levels/1/challenges/1)
    - [Git - Tutorial](http://www.vogella.com/tutorials/Git/article.html)
    - [Learn Git Branching](http://pcottle.github.io/learnGitBranching/)
    </div>
    <div class="col-md-4" markdown="1">
    ### Manual Pages

    - [Linux man pages online](http://man7.org/linux/man-pages/index.html)
    - [FreeBSD man pages](https://www.freebsd.org/cgi/man.cgi)
    - [POSIX Standard](http://pubs.opengroup.org/onlinepubs/9699919799/)
    </div>
    <div class="col-md-4" markdown="1">
    ### Communication

    - [Beej's Guide to Unix Interprocess Communication](http://beej.us/guide/bgipc/)
    - [Beej's Guide to Network Programming](http://beej.us/guide/bgnet/)
    </div>
    <div class="col-md-4" markdown="1">
    ### Pthreads

    - [POSIX Threads Programming](https://computing.llnl.gov/tutorials/pthreads/)
    - [Multithreaded Programming (POSIX pthreads Tutorial)](https://randu.org/tutorials/threads/)
    </div>
    </div>
