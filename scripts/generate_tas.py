#!/usr/bin/env python2.7

import csv
import random
import glob
import yaml

TAS      = [ta['netid'] for ta in yaml.load(open('static/yaml/ta.yaml'))]
STUDENTS = []

for path in glob.glob('static/csv/*.csv'):
    for student in csv.DictReader(open(path, 'rU')):
        STUDENTS.append(student['Netid'])

random.shuffle(STUDENTS)
random.shuffle(TAS)

TAS      = TAS * (len(STUDENTS) / len(TAS) + 1)
MAPPING  = list(sorted(map(list, zip(STUDENTS, TAS))))

print yaml.dump(MAPPING, default_flow_style=False)
